#include "StackV.h"
#include <iostream>

using namespace std;


int Stack::size()
{
	return data.size();
}

void Stack::push(int n)
{
	data.push_back(n);
}

void Stack::pop()
{
	data.pop_back();
}

int Stack::top()
{
	int x = data.size() -1;
	cout << "Elements at the top is: "<< x<< endl;
	return x;
}

void Stack::clear()
{
	data.clear();
}
